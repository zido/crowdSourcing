package com.hudongwx.crowdSourcing.controller;

import com.hudongwx.crowdSourcing.common.BaseController;
import com.hudongwx.crowdSourcing.model.Project;
import com.hudongwx.crowdSourcing.model.ProjectType;
import com.hudongwx.crowdSourcing.util.Common;
import com.jfinal.aop.Before;
import com.jfinal.ext.interceptor.POST;

import java.util.Date;
import java.util.List;

/**
 * Created by wuhongxu on 2016/11/22 0022.
 */
public class ProjectController extends BaseController {
    public void index() {
        fillHeaderAndFooter();
        Integer id = getParaToInt(0);
        setAttr("project", Project.dao.getProject(id));
        setAttr("random", Project.dao.getInvalidProject(Common.START_PAGE));
        render("index.ftl");
    }

    public void toAdd() {
        fillHeaderAndFooter();
    }

    public void editReward() {
        fillHeaderAndFooter();
        List<ProjectType> pts = ProjectType.dao.getAllProjectType();
        setAttr("types", pts);
    }

    @Before(POST.class)
    public void toPublish() {
        fillHeaderAndFooter();
        Project project = getModel(Project.class);
        project.setCreateTime(new Date());
        project.setUpdateTime(new Date());
        project.save();
        setAttr("project", project);
    }

    public void publish() {
        fillHeaderAndFooter();
        Integer id = getParaToInt("proj",-1);
        Project project = Project.dao.getProject(id);
        if(project == null)
            renderError(404);
        else{
            project.setInvalid(Common.VALUE_VISIBLE);
            project.update();
            redirect("/"+getAttr(Common.LABEL_STATIC_SERVE_PATH));
        }
    }
}
