package com.hudongwx.crowdSourcing.controller;

import com.baomidou.kisso.SSOConfig;
import com.baomidou.kisso.SSOHelper;
import com.baomidou.kisso.SSOToken;
import com.baomidou.kisso.common.util.HttpUtil;
import com.hudongwx.crowdSourcing.common.BaseController;

import java.io.IOException;

/**
 * Created by wuhongxu on 2016/11/22 0022.
 */
public class UserController extends BaseController {
    public void index() {
        renderText("...");
    }

    public void loginOut() throws IOException {
        SSOHelper.logout(getRequest(), getResponse());
        redirect("/");
    }

    public void login() throws IOException {
        SSOToken token = SSOHelper.getToken(getRequest());
        if (token != null) {
            redirect("/");
            return;
        }
        SSOHelper.clearRedirectLogin(getRequest(), getResponse());
        renderNull();
    }

    public void register() throws IOException {
        SSOConfig config = SSOHelper.getKissoService().getConfig();
        String loginUrl = config.getLoginUrl();
        String retUrl = HttpUtil.getQueryString(getRequest(), config.getEncoding());
        redirect(HttpUtil.encodeRetURL(loginUrl, config.getParamReturl(), retUrl) + "#");
    }
}
