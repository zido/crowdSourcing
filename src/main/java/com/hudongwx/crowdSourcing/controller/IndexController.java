package com.hudongwx.crowdSourcing.controller;

import com.baomidou.kisso.SSOHelper;
import com.baomidou.kisso.SSOToken;
import com.hudongwx.crowdSourcing.common.BaseController;
import com.hudongwx.crowdSourcing.model.Project;
import com.hudongwx.crowdSourcing.model.ProjectStatus;
import com.hudongwx.crowdSourcing.model.ProjectType;
import com.hudongwx.crowdSourcing.util.Common;

/**
 * Created by wuhongxu on 2016/11/21 0021.
 */
public class IndexController extends BaseController {
    public void index() {
        fillHeaderAndFooter();
        Integer p = getParaToInt('p', Common.START_PAGE);
        SSOToken token = SSOHelper.getToken(getRequest());
        setAttr("token", token);
        setAttr("page", Project.dao.getInvalidProject(p));
        setAttr("random", Project.dao.getInvalidProject(Common.START_PAGE));
        setAttr("types", ProjectType.dao.getAllProjectType());
        setAttr("status", ProjectStatus.dao.getAllProjectStatus());
        render("/common/index.ftl");
    }
}
