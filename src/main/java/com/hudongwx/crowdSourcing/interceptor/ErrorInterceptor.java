package com.hudongwx.crowdSourcing.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.core.JFinal;
import com.jfinal.log.Log;

/**
 * Created by wuhongxu on 2016/11/24 0024.
 */
public class ErrorInterceptor implements Interceptor {
    private Log log = Log.getLog(getClass());

    @Override
    public void intercept(Invocation inv) {
        try {
            inv.invoke();
        } catch (Exception e) {
            logWrite(inv,e);
        }
    }

    private void logWrite(Invocation inv, Exception e) {
        //开发模式
        if (JFinal.me().getConstants().getDevMode()) {
            e.printStackTrace();
        }
        log.error("\n---Exception Log Begin---\n" + "Controller:" + inv.getController().getClass().getName() + "\n" +
                "Method:" + inv.getMethodName() + "\n" +
                "Exception Type:" + e.getClass().getName() + "\n" +
                "Exception Details:", e);

    }
}
