package com.hudongwx.crowdSourcing.model;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;

/**
 * Generated by JFinal, do not modify this file.
 * <pre>
 * Example:
 * public void configPlugin(Plugins me) {
 *     ActiveRecordPlugin arp = new ActiveRecordPlugin(...);
 *     _MappingKit.mapping(arp);
 *     me.add(arp);
 * }
 * </pre>
 */
public class _MappingKit {

	public static void mapping(ActiveRecordPlugin arp) {
		arp.addMapping("project", "id", Project.class);
		arp.addMapping("project_status", "id", ProjectStatus.class);
		arp.addMapping("project_type", "id", ProjectType.class);
		arp.addMapping("user", "id", User.class);
	}
}

