package com.hudongwx.crowdSourcing.common;

import com.hudongwx.crowdSourcing.controller.IndexController;
import com.hudongwx.crowdSourcing.controller.ProjectController;
import com.hudongwx.crowdSourcing.controller.UserController;
import com.hudongwx.crowdSourcing.interceptor.ErrorInterceptor;
import com.hudongwx.crowdSourcing.interceptor.RequestHandler;
import com.hudongwx.crowdSourcing.model._MappingKit;
import com.hudongwx.crowdSourcing.sso.plugin.KissoJFinalPlugin;
import com.hudongwx.crowdSourcing.util.Common;
import com.jfinal.config.*;
import com.jfinal.core.JFinal;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.render.ViewType;

/**
 * Created by wuhongxu on 2016/11/21 0021.
 */
public class MainConfig extends JFinalConfig {
    /**
     * Config constant
     *
     * @param me
     */
    @Override
    public void configConstant(Constants me) {
        me.setDevMode(PropKit.use("config.properties").getBoolean("devMode"));
        me.setViewType(ViewType.FREE_MARKER);
        me.setFreeMarkerViewExtension("ftl");
        me.setBaseUploadPath("images");
    }

    /**
     * Config route
     *
     * @param me
     */
    @Override
    public void configRoute(Routes me) {
        me.add("/", IndexController.class);
        me.add("project", ProjectController.class);
        me.add("user", UserController.class);
    }

    /**
     * Config plugin
     *
     * @param me
     */
    @Override
    public void configPlugin(Plugins me) {
        Prop dataBaseProp = PropKit.use("local.properties");
        C3p0Plugin c3p0Plugin = new C3p0Plugin(dataBaseProp.get("jdbcUrl"),dataBaseProp.get("user"),dataBaseProp.get("password"));
        me.add(c3p0Plugin);

        ActiveRecordPlugin activeRecordPlugin = new ActiveRecordPlugin(c3p0Plugin);
        activeRecordPlugin.setShowSql(true);
        _MappingKit.mapping(activeRecordPlugin);
        me.add(activeRecordPlugin);

        me.add(new KissoJFinalPlugin());
    }

    /**
     * Config interceptor applied to all actions.
     *
     * @param me
     */
    @Override
    public void configInterceptor(Interceptors me) {
        me.add(new ErrorInterceptor());
        //me.add(new SSOIntercepter());
    }

    /**
     * Config handler
     *
     * @param me
     */
    @Override
    public void configHandler(Handlers me) {
        me.add(new RequestHandler());
        me.add(new ContextPathHandler(Common.LABEL_STATIC_SERVE_PATH));
    }

    public static void main(String[] args) {
        JFinal.start("src/main/webapp", 8082, "/", 5);
    }
}
