<div class="content bg-white far-top">
    <div class="container">
        <div class="row">
            <span class="pull-right far-left"><a href="/">互动直聘</a></span>
            <span class="pull-right far-left"><a href="/">互动论坛</a></span>
            <span class="pull-right far-left"><a href="/">关于我们</a></span>
        </div>
        <div>
            <div class="pull-right">
                Copyright © 2014-2016 互动无限科技有限公司
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="${staticServePath}/static/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="${staticServePath}/static/js/bootstrap.min.js"></script>