<#include "../macro-head.ftl">
<!DOCTYPE HTML>
<html>
<head>
<@head title="添加项目"></@head>
</head>
<body>
<#include "../macro-nav.ftl">
<div class="wrapper far-top">

    <div class="far-away-top content bg-white">
        <div class="row">
            <div class="col-md-4 text-center">
                <div class="progress-item active">
                    <div><span>第一步</span></div>
                    <div><span>选择项目类型</span></div>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="progress-item">
                    <div><span>第二步</span></div>
                    <div><span>填写信息</span></div>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="progress-item">
                    <div><span>第三步</span></div>
                    <div><span>发布信息</span></div>
                </div>
            </div>
        </div>
        <div class="progress far-top" style="width:100%">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60"
                 aria-valuemin="0" aria-valuemax="100" style="width: 33%;">
                33%
            </div>
        </div>
    </div>
    <div class="far-top content">

        <div class="row">
            <div class="col-md-4">
                <div class="bg-white big-item">
                    <div class="big-item-center-vertical">
                        <h1>悬赏</h1>
                        <a class="btn btn-lg btn-default" href="${staticServePath}/project/editReward">发布悬赏</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="bg-white big-item">
                    <div class="big-item-center-vertical">
                        <h1>整包</h1>
                        <a class="btn btn-lg btn-default">发布整包</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="bg-white big-item">
                    <div class="big-item-center-vertical">
                        <h1>兼职</h1>
                        <a class="btn btn-lg btn-default">发布兼职</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<#include "../macro-footer.ftl">
</body>
</html>