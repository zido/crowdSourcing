<#include "../macro-head.ftl">
<!DOCTYPE HTML>
<html>
<head>
<@head title="添加项目">

</@head>
</head>
<body>
<#include "../macro-nav.ftl">
<div class="wrapper far-top">

    <div class="far-away-top content bg-white">
        <div class="row">
            <div class="col-md-4 text-center">
                <div class="progress-item">
                    <div><span>第一步</span></div>
                    <div><span>选择项目类型</span></div>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="progress-item active">
                    <div><span>第二步</span></div>
                    <div><span>填写信息</span></div>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="progress-item">
                    <div><span>第三步</span></div>
                    <div><span>发布信息</span></div>
                </div>
            </div>
        </div>
        <div class="progress far-top" style="width:100%">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60"
                 aria-valuemin="0" aria-valuemax="100" style="width: 66%;">
                66%
            </div>
        </div>
    </div>
    <div class="far-top">
        <div class="edit-panel">
            <div class="edit-panel-in">
                <a class="res" href="${staticServePath}/project/toAdd">< 返回重选</a>
            </div>
            <div class="edit-panel-main">
                <div class="edit-panel-heading">
                    <h2>填写悬赏信息</h2>
                    <span>详细、明确的描述问题，能更好的让开发者了解你的需求，吸引更多的人来解决问题</span>
                </div>
                <div class="edit-panel-body">
                    <form class="form-horizontal" method="post" action="${staticServePath}/project/toPublish">
                    <#--表单完型部分-->
                        <div class="sr-only">
                            <label>
                                <input type="text" value="0" name="project.author_id">
                            </label>
                            <label>
                                <input id="content" class="sr-only" name="project.content"/>
                            </label>
                        </div>

                        <h3>填写基本信息</h3>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="title">项目名称：</label>
                            <div class="col-md-5">
                                <input class="form-control" name="project.title" id="title"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="type_id">项目分类：</label>
                            <div class="col-md-5">
                                <select class="form-control" name="project.type_id" id="type_id">
                                <#list types as type>
                                    <option value="${type.id}">${type.name}</option>
                                </#list>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">项目需求说明：</label>
                            <div class="col-md-9">
                                <div class="summernote"></div>
                            </div>
                        </div>
                        <h3>填写细节信息</h3>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="reward">项目预算：</label>
                            <div class="col-md-2">
                                <input class="form-control" id="reward" type="number" name="project.reward"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="period">项目周期：</label>
                            <div class="col-md-2">
                                <input class="form-control" id="period" type="number" name="project.period"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">上传附件：</label>
                            <div class="col-md-4">
                                <input class="form-control" id="period" type="file"/>
                            </div>
                        </div>
                        <h3>报名要求</h3>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="skills">技能要求：</label>
                            <div class="col-md-6">
                                <input class="form-control" id="skills" type="text" name="project.skills"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2"></div>
                            <div class="col-md-2">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="require_approver">要求实名认证
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="require_phone">要求手机验证
                                    </label>
                                </div>
                            </div>
                        </div>
                        <h3>您的个人信息</h3>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="per_name">姓名：</label>
                            <div class="col-md-4">
                                <input class="form-control" id="per_name" type="text" name="project.per_name"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="per_phone">电话：</label>
                            <div class="col-md-4">
                                <input class="form-control" id="per_phone" type="text" name="project.per_phone"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="per_email">邮箱：</label>
                            <div class="col-md-4">
                                <input class="form-control" id="per_email" type="text" name="project.per_email"/>
                            </div>
                        </div>
                        <button class="btn btn-primary btn-lg center-block" type="submit">发布信息</button>
                    </form>
                </div>
                <div class="edit-panel-footer text-center">

                </div>
            </div>

        </div>
    </div>

</div>
<#include "../macro-footer.ftl">
<script type="text/javascript" src="${staticServePath}/static/lib/summernote/summernote.min.js"></script>
<script type="text/javascript" src="${staticServePath}/static/lib/summernote/lang/summernote-zh-CN.min.js"></script>

<script type="text/javascript" src="${staticServePath}/static/lib/fileInput/fileinput.js"></script>
<script type="text/javascript" src="${staticServePath}/static/lib/fileInput/zh.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.summernote').each(function () {
            $(this).summernote({
                height: 300,
                lang: 'zh-CN',
                callbacks: {
                    onBlur: function () {
                        $('#content').val($(this).summernote('code'));
                    }
                }
            })
        });
        $('[type="file"]').each(function () {
            $(this).fileinput({
                lang: 'zh',
                allowedFileExtensions: ['jpg', 'png', 'gif'],
                showUpload: false, //是否显示上传按钮
                showCaption: false,//是否显示标题
                browseClass: "btn btn-primary", //按钮样式
                previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
            });
        })
    });
</script>
</body>
</html>