<#include "../macro-head.ftl">
<!DOCTYPE HTML>
<html>
<head>
<@head title="预览"></@head>
</head>
<body>
<#include "../macro-nav.ftl">
<div class="wrapper far-top">
    <div class="far-away-top content bg-white">
        <div class="row">
            <div class="col-md-4 text-center">
                <div class="progress-item">
                    <div><span>第一步</span></div>
                    <div><span>选择项目类型</span></div>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="progress-item">
                    <div><span>第二步</span></div>
                    <div><span>填写信息</span></div>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="progress-item active">
                    <div><span>第三步</span></div>
                    <div><span>发布信息</span></div>
                </div>
            </div>
        </div>
        <div class="progress far-top" style="width:100%">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60"
                 aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                100%
            </div>
        </div>
    </div>
    <div class="far-top content bg-white">
        <div class="bottom-border">
            <div class="text-center">
                <h1>预览</h1>
            </div>
            <div class="far-top">
                <div class="project-item">
                    <div class="title">项目属性</div>
                    <div class="content">
                        <div class="project-attr">
                            项目周期 :${project.period}
                        </div>
                    <#if project.skills??>
                        <div class="project-attr">
                            技能要求 :${project.skills}
                        </div>
                    </#if>
                    </div>
                </div>
                <div class="project-item">
                    <div class="title">项目描述</div>
                ${project.content}
                </div>
            </div>
        </div>
        <div class="far-top text-center">
            <a class="btn btn-primary btn-lg" href="${staticServePath}/project/publish?proj=${project.id}">发布</a>
        </div>
    </div>
</body>
</html>