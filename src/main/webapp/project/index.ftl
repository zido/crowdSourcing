<#include "../macro-head.ftl">
<!DOCTYPE HTML>
<html>
<head>
<@head title="${project.id}"></@head>
</head>
<body>
<#include "../macro-nav.ftl">
<div class="wrapper far-top">
    <div class="row">
        <div class="col-md-8">
            <div class="input-group input-group-lg">
                <label class="sr-only" for="whole-search">搜索</label>
                <input id="whole-search" type="text" class="form-control input-lg">
                <span class="input-group-btn">
                <button class="btn btn-primary btn-lg board" type="button">搜索</button>
                <span></span>
                </span>
            </div>
            <div class="far-away-top content bg-white">
                <div class="row">
                    <div class="col-md-2 text-center">
                        <div class="progress-item">
                            <div><span>发布</span></div>
                            <div><span>1992-05-03</span></div>
                        </div>
                    </div>
                    <div class="col-md-2 text-center">
                        <div class="progress-item">
                            <div><span>审核</span></div>
                            <div><span>1992-05-03</span></div>
                        </div>
                    </div>
                    <div class="col-md-2 text-center">
                        <div class="progress-item">
                            <div><span>托管资金</span></div>
                            <div><span>1992-05-03</span></div>
                        </div>
                    </div>
                    <div class="col-md-2 text-center">
                        <div class="progress-item active">
                            <div><span>竞标</span></div>
                            <div><span>1992-05-03</span></div>
                        </div>
                    </div>
                    <div class="col-md-2 text-center">
                        <div class="progress-item">
                            <div><span>实施</span></div>
                            <div><span>1992-05-03</span></div>
                        </div>
                    </div>
                    <div class="col-md-2 text-center">
                        <div class="progress-item">
                            <div><span>完成</span></div>
                            <div><span>1992-05-03</span></div>
                        </div>
                    </div>
                </div>
                <div class="progress far-top" style="width:100%">
                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60"
                         aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                        60%
                    </div>
                </div>
            </div>
            <div class="far-top content bg-white">
                <div class="bottom-border">
                    <div class="row">
                        <div class="col-md-9">
                            <h3 class="ellipsis">${project.title?html}</h3>
                            <div>
                                <a href="" class="text-primary">@${project.author_id}</a>
                                <span>发布于 : ${project.createTime}</span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="text-right">
                                <span class="big-money z-money-cny z-money-unit-only">${project.reward}</span>
                            </div>
                            <div class="text-right">
                            <span class="text-gray">
								300元/天&nbsp;×&nbsp;10天
							</span>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <a class="pull-right btn btn-primary btn-radius far-left">报名</a>
                        <a class="pull-right btn btn-default btn-radius"><span class="glyphicon glyphicon-heart-empty" style="position:relative;top:2px;"></span>&nbsp;收藏</a>
                    </div>
                </div>
                <div class="far-top">

                    <div class="project-item">
                        <div class="title">项目属性</div>
                        <div class="content">
                            <div class="project-attr">
                                项目周期 :${project.period}
                            </div>
                        <#if project.skills??>
                            <div class="project-attr">
                                技能要求 :${project.skills}
                            </div>
                        </#if>
                        </div>
                    </div>
                    <div class="project-item">
                        <div class="title">项目描述</div>
                    ${project.content}
                    </div>

                </div>
            </div>
        </div>
    <#include "../macro-right.ftl">
    </div>
</div>
</body>
</html>