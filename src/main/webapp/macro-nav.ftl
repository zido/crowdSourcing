<div class="nav bg-white">
    <div class="wrapper">
        <div class="row">

            <div class="col-md-2"><a href="${staticServePath}/" style="text-decoration: none;color: #333;"><img
                    class="logo" src=""></a></div>
            <!--主导航start-->
            <div class="col-md-8">
                <div class="box-aw box level">
                    <div class="menu-item active">
                        <a class="menu-item-header style-none" href="${staticServePath}/">找活</a>
                    </div>

                    <div class="menu-item ">
                        <a class="menu-item-header style-none" href="${staticServePath}">找人</a>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
            <#if isLogin>
                <span>${user.id}</span>
                <a href="${staticServePath}/user/loginOut">登出</a>
            <#else>
                <a href="${staticServePath}/user/login">登录/注册</a>
            </#if>

            </div>

        </div>
    </div>
</div>