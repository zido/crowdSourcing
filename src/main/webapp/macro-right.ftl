<div class="col-md-4">
    <div class="content-lg bg-white">
        <a class="btn btn-primary btn-lg center-block" href="${staticServePath}/project/toAdd">发布需求</a>
    </div>
    <div class="far-top content bg-white">
        <div class="right-header">
            <h3>推荐项目</h3>
        </div>
        <div class="banner-body">
        <#list random.list as proj>
            <div class="banner-item row">
                <div class="col-md-2">
                            <span class="item-fr <#if proj_index < 3>item-fr-active</#if>">
                            ${proj_index+1}
                            </span>
                </div>
                <div class="col-md-7 ellipsis">${proj.title?html}</div>
                <div class="col-md-3 text-red ellipsis">${proj.reward}</div>
            </div>
        </#list>
        </div>
    </div>
</div>