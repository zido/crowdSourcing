<#include "../macro-head.ftl">
<!DOCTYPE HTML>
<html>
<head>
<@head title="首页"></@head>
</head>
<body>
<#include "../macro-nav.ftl">
<div class="wrapper far-top">
    <div class="row">
        <div class="col-md-8">
            <div class="input-group input-group-lg">
                <label class="sr-only" for="whole-search">搜索</label>
                <input id="whole-search" type="text" class="form-control input-lg" value="">
                <span class="input-group-btn">
                <button class="btn btn-primary btn-lg board" type="button">搜索</button>
                <span></span>
                </span>
            </div>
            <div class="far-top">
                <span><img src="${staticServePath}/static/image/u267.png" style="width:17px;height:17px"/>热门标签：</span>
            </div>
            <div class="content bg-white far-top">
                <div class="banner">
                    <div class="clearfix">
                        <div class="row">

                            <div class="col-md-2 text-right"><label>项目分类：</label></div>
                            <div class="col-md-8 range">
                            <#list types as type>
                                <label class="check-item">
                                    <input type="checkbox"/>${type.name}
                                </label>
                            </#list>
                            </div>
                            <div class="col-md-2">
                                <div class="zh_more"><a class="open-range">展开<span
                                        class="glyphicon glyphicon-chevron-down"></span></a></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 text-right"><label>项目状态：</label></div>
                            <div class="col-md-8 range">
                            <#list status as statu>
                                <label class="check-item">
                                    <input type="checkbox"/>${statu.name}
                                </label>
                            </#list>
                            </div>
                            <div class="col-md-2">
                                <div class="zh_more"><a class="open-range">展开<span
                                        class="glyphicon glyphicon-chevron-down"></span></a></div>
                            </div>
                        </div>
                    </div>
                <#--<div class="row">
                    <div class="col-md-2 text-right"><label>项目金额：</label></div>
                    <div class="Search_jobs_sub">
                        <a href="/"
                           class="Search_jobs_sub_a Search_jobs_sub_cur">网站开发</a>
                        <a href="/"
                           class="Search_jobs_sub_a  ">HTML5应用</a>
                        <a href="/"
                           class="Search_jobs_sub_a  ">微信应用</a>
                        <a href="/"
                           class="Search_jobs_sub_a  ">移动开发</a>
                    </div>
                    <div class="zh_more"><a href="">展开<span class="glyphicon glyphicon-chevron-down"></span></a>
                    </div>
                </div>-->
                </div>
                <ul class="clearfix" style="padding-left: 0; ">
                    <li class="pull-left">
                        <div class="banner-head banner-active">进行中</div>
                    </li>
                    <li class="pull-left">
                        <div class="banner-head">已完成</div>
                    </li>
                </ul>
            </div>
            <div class="far-bottom"><span><a href="/">默认排序</a></span></div>
        <#list page.list as project>
            <div class="item row">
                <div class="col-md-4">
                    <img class="big-img-none" src=""/>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12 ellipsis">
                            <a href="/project/${project.id}" class="title style-none">${project.title?html}</a>
                        </div>
                    </div>
                    <div class="row">
                        <span class="tag">java</span>
                        <span class="tag">html</span>
                    </div>
                    <div class="row" style="height: 50px;overflow: hidden">
                        <p>
                        ${project.content}
                        </p>
                    </div>
                    <div class="row">
                        <span class="small">发布者：<a href="/">${project.authorId}</a></span>
                        <span class="far-left small">围观：${project.attention}</span>
                        <span class="small">发布时间：${project.createTime}</span>
                        <span class="small">修改时间：${project.updateTime}</span>
                    </div>
                </div>
            </div>
        </#list>
        </div>
    <#include "../macro-right.ftl">
    </div>
</div>
<#include "../macro-footer.ftl">
<script type="text/javascript">
    var open_range = function () {
        var dom = $(this).parent().parent().prev();
        if (dom.hasClass('range'))
            dom.removeClass('range');
        else
            dom.addClass('range');
    };
    $(function () {
        $('a.open-range').on('click', open_range);
    })
</script>
</body>
</html>